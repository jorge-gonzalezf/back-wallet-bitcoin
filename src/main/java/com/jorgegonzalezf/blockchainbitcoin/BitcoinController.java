package com.jorgegonzalezf.blockchainbitcoin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class BitcoinController {
	
	private final String URL = "http://localhost:3000/merchant/";
	
	@Autowired
    RestTemplate restTemplate;
	
	@CrossOrigin
	@RequestMapping("/fetching-the-wallet-balance")
    public String validate(@RequestParam String guid, @RequestParam  String password) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
        
        org.json.simple.JSONObject jsonSimple = new org.json.simple.JSONObject();
        jsonSimple.put("password", password);
        HttpEntity<String> entity = new HttpEntity<>(jsonSimple.toJSONString(), headers);
		
        return restTemplate.exchange(
                URL + guid + "/balance",
                HttpMethod.POST, entity, String.class).getBody();
	}
	
	@CrossOrigin
	@RequestMapping("/make-payment")
    public String payment(@RequestParam String guid, @RequestParam  String password, @RequestParam String to, @RequestParam  String amount) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
        
        org.json.simple.JSONObject jsonSimple = new org.json.simple.JSONObject();
        jsonSimple.put("password", password);
        jsonSimple.put("second_password", "");
        jsonSimple.put("to", to);
        jsonSimple.put("amount", amount);
        HttpEntity<String> entity = new HttpEntity<>(jsonSimple.toJSONString(), headers);
		
        return restTemplate.exchange(
                URL + guid + "/payment",
                HttpMethod.POST, entity, String.class).getBody();
	}
	
	@CrossOrigin
	@RequestMapping("/get-developer-profile")
    public String getDeveloperProfile(@RequestParam String user) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
		
        return restTemplate.exchange(
                "https://api.github.com/users/" + user,
                HttpMethod.GET, entity, String.class).getBody();
	}
}
