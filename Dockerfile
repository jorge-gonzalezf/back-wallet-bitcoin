FROM openjdk:8
VOLUME /tmp
EXPOSE 8818
ADD ./target/blockchain-bitcoin-0.0.1-SNAPSHOT.jar blockchain-bitcoin.jar
ENTRYPOINT ["java","-jar","/blockchain-bitcoin.jar"]